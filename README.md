# Extreme Parallelism

Investigating the effect of running many solvers in parallel for a given instance instead of sequentially picking only a few or one, guaranteeing the best possible solver is chosen.