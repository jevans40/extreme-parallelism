#!usr/bin/env python
#File Version 1.0 10/11/19 12 PM
import plotly.express as px
import plotly
import glob
import sys
import statistics
import pandas as pd

Version_Number = 1.0



#-------------------------What this script does-----------------------------------------
#This script takes a directory of output files from slurm.error and scrapes the runtime.
#Runtime must be gathered using the linux Time command. Other formats may not be supported.
#This data is then processed into pandas dataframes and saved as CSV files in the following
#format:

#InstanceName,Solver,ParallelProcesses,Partition,Mean,Median,Mode,StandardDeviation,ErrorTime,ErrorMemory,ErrorUndefined
#Some notes:
#Data will be NA for all numerical categories if the data has an error


#Command to run this script:
#python DataScraper.py Directory_To_Scrape/ Runlog.txt AppendCSV

#Where Directory_To_Scrape/ is a directory that containes .error files with runtimes in them.
#Runlog.txt is the runlog associated with the directory being graphed.
#AppendCSV is the file to append the data to. If left blank the program will create a new file.
#This new CSV File will be called DefaultCSVOut.csv

#Additional Options


#Simple function to return wether or not the data is numeric
def isNumeric(S):
    try:
        float(S)
        return True
    except ValueError:
        return False



#Returns a list of tuples from a string with runtimes.
# Format:
# [[minute,second],[minute,second],...]
def scrapeData(toScrapeData):
    toScrapeData = toScrapeData.split()
    mark = 0
    currentTuple = []
    listOfTuples = []
    #input(toScrapeData)
    for i in toScrapeData:

        #Find the runtimes in the file
        if mark == 1:
            currentTuple.append(i)
            listOfTuples.append(currentTuple)
            currentTuple = []
            mark = 0
        if mark == 2:
            currentTuple.append(i)
            mark = 1
        if i == "real":
            mark = 2
    return listOfTuples

def MakeDataElement(InstanceName,Solver,ParallelProcesses,Partition,Mean,Median,Mode,StandardDefiation,ErrorTime,ErrorMemory,ErrorUndefined):
  if(ErrorTime or ErrorMemory or ErrorUndefined):
    return [InstanceName,Solver,ParallelProcesses,Partition,"NA","NA","NA","NA",ErrorTime,ErrorMemory,ErrorUndefined]
  else:
    return [InstanceName,Solver,ParallelProcesses,Partition,Mean,Median,Mode,StandardDeviation,ErrorTime,ErrorMemory,ErrorUndefined]

#-----------------------------------PROGRAM START-----------------------------------
#Dir is the directory we are getting data from
Dir =  sys.argv[1]

#Runlog data is associated with
RunLog = sys.argv[2]

#Find if the user specified a CSV file or not, and set it
CSV = "DefaultCSVOut.csv"
if(len(sys.argv) > 3):
  CSV = sys.argv[3]

DirData = Dir.replace("-",' ').replace(".",' ').replace(Dir," ").split()
Solver  = "\0"
Partition = "\0"
print(DirData)
for i in range(len(DirData)):
  if DirData[i] == "Cores":
    Partition = DirData[i+1]
  if DirData[i] == "Partition":
    Solver = DirData[i+1]


RunlogDataFrame = pd.read_csv(RunLog,header = None)


print("Scraping the Directory ", Dir," With Script Version ", Version_Number)

Times = []

FullData = [["InstanceName","Solver","ParallelProcesses","Partition","Mean","Median","Mode","StandardDeviation","TimeError","MemoryError","UndefinedError"]]

print(Solver + Partition)
input()
#For every file in the given directory that ends in .error
for file in glob.glob("./{}/*.error".format(Dir)):
    fileText = ""
    Runtime = 0
    RunSum = 0
    Out_Of_Memory_Error =  False
    Time_Out_Error = False
    Undefined_Error = False
    with open(file, "r") as data:

        data = data.read()
        
        #Error Checking, make sure there is no timeouts
        if not (data.find("oom-kill") == -1):
            Out_Of_Memory_Error = True
        if not (data.find("TIME") == -1):
            Time_Out_Error = True

        data = data.replace("m"," ")
        data = data.replace("s"," ")
        fileText = data.replace('\n',' ')

    #Get the Test ID and how many Parallel solvers
    filename = file.replace("TestNum",' Mark ').replace("-",' ').replace(".",' ').replace(Dir," ").split()
    #TestNumber, and Core count [File 1 File 2]
    TestID = 0
    ParallelSolvers = 0

    #Get the TestID and Parallel Solvers from the filename
    for i in range(len(filename)):
        if filename[i] == "Mark":
            TestID = filename[i+1]
            ParallelSolvers = filename[i+2]
            break

    #If nothing was found then flag it
    if not fileText:
        Undefined_Error = True
        FullData.append(MakeDataElement(RunlogDataFrame[RunlogDataFrame[2] == TestID][0][1],Solver,ParallelSolvers,Partition,0,0,0,0,Time_Out_Error,Out_Of_Memory_Error,Undefined_Error))
        continue
       

    fileText = scrapeData(fileText)

    #If the file has no runtimes flag it
    if len(fileText) == 0:
        Undefined_Error = True
        FullData.append(MakeDataElement(RunlogDataFrame[RunlogDataFrame[2] == TestID][0][1],Solver,ParallelSolvers,Partition,0,0,0,0,Time_Out_Error,Out_Of_Memory_Error,Undefined_Error))
        continue
    
    #Median of the runs
    Medians = []
    for i in fileText:
        Medians.append(float(i[0]) *60 + float(i[1]))
    
    MedianRuntime = statistics.median(Medians)
    
    #Just find the testNum in the file list and use that to index the file names! no need for regexp
    #Push Test with average runtime on the Times list
    #Times.append([ParallelSolvers,TestID,Runtime])
    #Push Test with median of runtimes on the Times list
    Times.append([ParallelSolvers,TestID,MedianRuntime])

Data = []

for i in Times:
   newCategory =  True
   for k in Data:
       if k[0] == i[1]:
          newCategory = False
   if newCategory == True:
          Data.append([i[1]])
   for k in Data:
       if k[0] == i[1]:
          k.append([i[0],i[2]])
#Data is in the format [TestNum,[InstanceNum,Time]]

#Put the runtimes into Data as long as all tests finished the test
for i in range(len(Data)):
    #print(Data[i][0])
    Data[i][0] = int(Data[i][0])
Data.sort()
for i in range(len(Data)):
    #print(Data[i][0])
    Data[i][0] = [0,int(Data[i][0])]

for i in Data:
    ToRemove = []
    for k in i:
        inset = False
        for j in Data[-1]:
            if k[0] == j[0]:
               inset = True
        if not inset:
           ToRemove.append(k)
    for k in ToRemove:
        i.remove(k)   
#Remove tests that didnt finish on atleast once instance
Traces = []




#Sanity check:
print("Starting sanity check: ")
print("There are ",len(Data)," tests in this batch.")
print("All of them should have the same number of data points")
print("Test , DataPoints")
for i in range(len(Data)):
    print(Data[i][0], " , " ,len(Data[i]))
    for k in range(len(Data[i])):
        Data[i][k][0] = int(Data[i][k][0])
Data[0].sort()
Data[-1].sort()
#print(Data[0])
#print(Data[-1])

for i in range(len(Data)):
    #print(Data[i][0])
    Data[i][0] = int(Data[i][0][1])

tips = px.data.tips()
print(tips)



#Graph the data
for i in Data:
    Traces.append(px.box([j[1] for j in i[1:]],y = "Time in seconds", x = "{} Parallel Solvers".format(i[0])))
plotly.offline.plot(Traces)

