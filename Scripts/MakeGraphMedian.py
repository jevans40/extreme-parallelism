#!usr/bin/env python
#File Version 1.1 9/13/19 10:09 AM
import plotly.express as px
import plotly
import glob
import sys
import statistics

Version_Number = 1.1
Out_Of_Memory_Error =  False
Time_Out_Error = False

#Returns wether or not the data is numeric
def isNumeric(S):
    try:
        float(S)
        return True
    except ValueError:
        return False

Dir =  sys.argv[1]
Outputfile = sys.argv[1]
print("Graphing the Directory ", Dir," With Script Version ", Version_Number)


#Returns a list of tuples [[m,s],[m,s]]
def scrapeData(toScrapeData):
    toScrapeData = toScrapeData.split()
    mark = 0
    currentTuple = []
    listOfTuples = []
    #input(toScrapeData)
    for i in toScrapeData:

        #Find the runtimes in the file
        if mark == 1:
            currentTuple.append(i)
            listOfTuples.append(currentTuple)
            currentTuple = []
            mark = 0
        if mark == 2:
            currentTuple.append(i)
            mark = 1
        if i == "real":
            mark = 2
    return listOfTuples


Times = []

#For every file in the given directory that ends in .error
for file in glob.glob("./{}/*.error".format(Dir)):
    fileText = ""
    Runtime = 0
    RunSum = 0
    with open(file, "r") as data:
        
        data = data.read()

        #Error Checking, make sure there is no timeouts
        if not (data.find("oom-kill") == -1):
            Out_Of_Memory_Error = True
        if not (data.find("TIME") == -1):
            Time_Out_Error = True

        if(Out_Of_Memory_Error or Time_Out_Error):
            if(Out_Of_Memory_Error):
            #    print("Found Out Of Memory Error")
                Out_Of_Memory_Error = False
            if(Time_Out_Error):
            #    print("Found Time Out Error")
                Time_Out_Error = False
            continue

        data = data.replace("m"," ")
        data = data.replace("s"," ")
        fileText = data.replace('\n',' ')

    if not fileText:
       continue
    #Scrape the data then calculate the average
    fileText = scrapeData(fileText)

    #If a oom-kill was detected (out of memory) then skip this test
    if Out_Of_Memory_Error:
        Out_Of_Memory_Error = False
        continue

    #If the file has no runtimes, just continue
    if len(fileText) == 0:
        continue

    #Average the runs
#    for i in fileText:
#        RunSum = RunSum + float(i[0]) * 60 + float(i[1])
#    Runtime = RunSum/len(fileText)
    
    #Median of the runs
    Medians = []
    for i in fileText:
        Medians.append(float(i[0]) *60 + float(i[1]))
    
    MedianRuntime = statistics.median(Medians)
    
    #Get the Test ID and how many Parallel solvers
    filename = file.replace("TestNum",' Mark ').replace("-",' ').replace(".",' ').replace(Dir," ").split()
    #TestNumber, and Core count [File 1 File 2]
    TestID = 0
    ParallelSolvers = 0

    #Get the TestID and Parallel Solvers from the filename
    for i in range(len(filename)):
        if filename[i] == "Mark":
            TestID = filename[i+1]
            ParallelSolvers = filename[i+2]
            break
    #Just find the testNum in the file list and use that to index the file names! no need for regexp
    #Push Test with average runtime on the Times list
    #Times.append([ParallelSolvers,TestID,Runtime])
    #Push Test with median of runtimes on the Times list
    Times.append([ParallelSolvers,TestID,MedianRuntime])
Data = []

for i in Times:
   newCategory =  True
   for k in Data:
       if k[0] == i[1]:
          newCategory = False
   if newCategory == True:
          Data.append([i[1]])
   for k in Data:
       if k[0] == i[1]:
          k.append([i[0],i[2]])
#Data is in the format [TestNum,[InstanceNum,Time]]

#Put the runtimes into Data as long as all tests finished the test
for i in range(len(Data)):
    #print(Data[i][0])
    Data[i][0] = int(Data[i][0])
Data.sort()
for i in range(len(Data)):
    #print(Data[i][0])
    Data[i][0] = [0,int(Data[i][0])]

for i in Data:
    ToRemove = []
    for k in i:
        inset = False
        for j in Data[-1]:
            if k[0] == j[0]:
               inset = True
        if not inset:
           ToRemove.append(k)
    for k in ToRemove:
        i.remove(k)   
#Remove tests that didnt finish on atleast once instance
Traces = []




#Sanity check:
print("Starting sanity check: ")
print("There are ",len(Data)," tests in this batch.")
print("All of them should have the same number of data points")
print("Test , DataPoints")
for i in range(len(Data)):
    print(Data[i][0], " , " ,len(Data[i]))
    for k in range(len(Data[i])):
        Data[i][k][0] = int(Data[i][k][0])
Data[0].sort()
Data[-1].sort()
#print(Data[0])
#print(Data[-1])

for i in range(len(Data)):
    #print(Data[i][0])
    Data[i][0] = int(Data[i][0][1])

tips = px.data.tips()
print(tips)



#Graph the data
for i in Data:
    Traces.append(px.box([j[1] for j in i[1:]],y = "Time in seconds", x = "{} Parallel Solvers".format(i[0])))
plotly.offline.plot(Traces)

