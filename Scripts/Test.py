#imports
import glob
import subprocess
import sys
import datetime
import os

#Version 1.0
'''
This program runs a sets of parallel processes and reports the time they took to finish.
This program requires the following input file in order to work!

FILE FORMAT::
SatSolver
PartitionName
Directory of Test Instances
Number of Parallel Processes, comma seperated (ex. 1, 2, 3  for 1 2 and 3 Parallel processes per test)
File Extension
Command Line Arguments for the solver
'''



def SubmitJob(SatSolver,Partition,Node,Test,TestInstances,Path,Argument):
    '''
    Submits a test set to the cluster given the following inputs:

    SatSolver - What SatSolver to use
    Partition - What Partition to use
    Node - Specified nodes #Depricated
    Test - The Number of Solvers to run in parallel
    TestInstances - A list of Instances to run tests on
    Path - Path to output folder
    Argument - Arguments to run with the given solver 
    '''
    returnString = []
    
    pool = 0
    TestString = "{ "
    StartInstance = 0
    for i in range(len(TestInstances)): 
        for k in range(int(Test)):
            TestString += "echo {}; ".format(os.getcwd() + "/" + TestInstances[i])
        TestString +="} "
        with  open (Path + "/jobs/" + "TestNum{}-{}".format(Test,i),"w") as file:
                  file.write('#!/bin/bash'+'\n')
                  file.write('#SBATCH --chdir="{}"'.format(os.getcwd() + '/' + Path)+ '\n')
                  file.write('#SBATCH --account=mallet'+'\n')
                  file.write('#SBATCH --time=0-00:30:00'+'\n')
                  file.write('#SBATCH --partition={}'.format(Partition)+'\n')
                  file.write('#SBATCH --job-name=TestNum{}-{}'.format(Test,i)+ '\n')
                  file.write('#SBATCH --output=./TestNum{}-{}.output'.format(Test,i) + '\n')
                  file.write('#SBATCH --error=./TestNum{}-{}.error'.format(Test,i) + '\n')
                  file.write('#SBATCH --exclusive' + '\n')
                  file.write('module load gcc' + '\n')
                  file.write('module load parallel' + '\n')
                  file.write('{} | parallel time {} {} {{}}'.format(TestString, os.getcwd() + '/' + SatSolver, Argument))

        #print("Running Test {} on {}".format(SatSolver,Test))
        pool = 0
        TestString = "{ "
           
        subprocess.run(['sbatch', '{}/jobs/TestNum{}-{}'.format(Path,Test,i)])
        returnString.append("{NumOfNodes},{Instance},{TestNumber}".format(NumOfNodes=Test,Instance=TestInstances[i],TestNumber=i))
    return '\n'.join(returnString)

ConfigurationFileName = sys.argv[1]
SatSolver = ""
Partition = ""
Nodes = []
Tests = []

print("Starting up sat solvers based on the following configureation file {}".format(ConfigurationFileName))


#Get Data from the file provided at input
Data = ""
with open(ConfigurationFileName, "r") as file:
    Data = file.read()
    print(Data)

Data = Data.split('\n')

#Save the data into the file
SatSolver = Data[0]
Partition = Data[1]
InstancesDirectory = Data[2]
Tests = Data[3].split(',')
FileExtensions = Data[4].split(',')
Arguments = Data[5]

print("Nodes :", Nodes)
print("CWD = ",os.getcwd())
for i in FileExtensions:
	TestInstances = glob.glob(InstancesDirectory+ "*" + i)
#print(TestInstances)

log = []
Path = '{}-Cores-{}-Partition-{}-Solver-{}'.format('-'.join(Tests),Partition,SatSolver,date.strftime("%B-%d-%Y")).replace('.','').replace('/','')
os.mkdir(Path)
os.mkdir(Path + "/jobs")

for i in range(len(Tests)):
   log.append(SubmitJob(SatSolver,Partition,1,Tests[i],TestInstances,Path,Arguments))


SatSolver = SatSolver.replace('.','')

SatSolver = SatSolver.replace('/','')

date = datetime.datetime.now()

with open('RunLog-{}-Cores{}-Partition-{}-Solver-{}.txt'.format('-'.join(Tests),Partition,SatSolver,date.strftime("%B-%d-%Y")),"w") as file:
    file.write('\n'.join(log))